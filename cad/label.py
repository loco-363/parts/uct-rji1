# ***********************************************
# ***      Loco363 - Parts - UCT RJI16x       ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class UCT_RJI16x(generic.bases.LabelPart):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			223,
			158,
			center=True,
			properties={
				'fill': self.COLOR_UNI,
				'stroke': self.color,
				'stroke-width': 3
			}
		))
		
		self.add(gen_draw.shapes.Rectangle(
			c,
			213,
			148,
			center=True,
			properties={
				'fill': self.COLOR_UNI,
				'stroke': self.color,
				'stroke-width': 0.5
			}
		))
		
		# indicator area
		self.add(gen_draw.shapes.Rectangle(
			C(c, (-223/2+15, -158/2+43)),
			130,
			100,
			properties={
				'fill': 'white'
			}
		))
		
		# keyboard area
		self.add(gen_draw.shapes.Rectangle(
			C(c, (223/2-15-53, -158/2+15)),
			53,
			128,
			properties={
				'fill': 'white'
			}
		))
		
		# display area
		cd = C(c, (-223/2+30+50, -158/2+17+7))
		self.add(gen_draw.shapes.Rectangle(
			cd,
			100,
			14,
			center=True,
			properties={
				'fill': 'white'
			}
		))
		
		# push-buttons
		for i in (-1, 1):
			for j in (-1, 1):
				self.add(gen_draw.shapes.Circle(
					C(cd, (i*(50+15), j*7)),
					3,
					properties={
						'fill': 'white'
					}
				))
	# constructor
# class UCT_RJI16x


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(UCT_RJI16x(name='RJI16x'), True))
